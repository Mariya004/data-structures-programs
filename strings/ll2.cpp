#include<iostream>
using namespace std;

class Node {
    int data;
    Node* link;
public:
    Node(int data) {
        this->data = data;
        this->link = NULL;
    }
    int get_data() {
        return data;
    }
    void set_link(Node* link) {
        this->link = link;
    }
    Node* get_link() {
        return link;
    }
    void set_data(int data) {
        this->data = data;
    }
};

class LinkedList {
    Node* head;
public:
    LinkedList() {
        head = NULL; // Initialize head to NULL
    }
    void create_LL() {
        string op;
        do {
            cout << "Enter the value:";
            int x;
            cin >> x;
            Node* node = new Node(x); // Define node inside the loop
            if (head == NULL) {
                head = node;
            } else {
                Node* current = head;
                while (current->get_link() != NULL) {
                    current = current->get_link();
                }
                current->set_link(node);
            }
            cout << "Do you wanna add number(y/n):";
            cin >> op;
        } while (op == "y");
    }
    
    // Add other necessary methods like display, destructor, etc. here
};

int main() {
    LinkedList ll;
    ll.create_LL();
    // You can add other function calls or operations related to the linked list here
    return 0;
}
