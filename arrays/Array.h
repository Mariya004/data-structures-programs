#include<iostream>
#include<ostream>
using namespace std;
template <class T>
class Array{
	int LB,UB;
	T A[100];
  public:
  	Array();
  	Array(int,int,T[]);
  	void insert_at_end(T);
  	void insert_at_beg(T);
  	void insert_at_index(T,int);
  	void delete_at_beg();
  	void delete_at_end();
  	void delete_at_ind(int);
  	T linear_search(T); 
  	void swap(int,int);
  	void selection_sort();
  	void bubble_sort();
  	T binary_search(T);
  	void insertion_sort();
  	int partition(int,int);
  	void quick_sort(int,int);
  	void merge(int,int,int);
  	void merge_sort(int,int);
  	int count(T);
  	void rotate_clockwise(int);
  	void rotate_anticlockwise(int);
  	void frequency();
  	void distinct();
  	
  	
  	
  	
 	template <class U>
  	friend ostream& operator<<(ostream&, Array<U> );
};

